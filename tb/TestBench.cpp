#include "TestBench.h"

using namespace emu_lib;

void info_handler (void* context, SceMiIC* ic);
void error_handler(void* context, SceMiEC* ec);


/**********************************************************************************************************************************************/
/*
 * This is calback function to inform TB that MessageInPort is ready to receive a new message.
 * This function is called automatically by SceMi API when receiveReady port is high.
 * */
void messageInPortProxyIsReadyCallback( void *_context ) 
{
	/*Inform TB that transactor is ready to receive a new message*/
	static_cast< TestBench* >( _context )->m_inPortReady++;
}
/**********************************************************************************************************************************************/
/*
 * This is calback function to that there is a new message from MessageOutPort.
 * */
void messageOutPortProxyreceiveCallback( void *_context, const SceMiMessageData *_data ) 
{
	TestBench* tb = static_cast< TestBench* >( _context );
	/*Read a new data from transactor*/
	tb->m_readData = _data->Get( 1 );
	tb->m_readData = (tb->m_readData << 32) | _data->Get( 0 );
	/*Inform TB that a new data from transactor is ready to use*/
	tb->m_outPortDataReady++;
}
/**********************************************************************************************************************************************/
/*
 * TestBench class constructor.
 * */
TestBench::TestBench( const char * _scemiXmlFile ) 
	: m_params( NULL )
	, m_inPortMessage_mp_out( NULL )
	, m_readData( 0 )
	, m_scemiFile( _scemiXmlFile )
	, m_inPortReady( 0 )
	, m_outPortDataReady( 0 )
{
	/* Bind message ports with proxy and initialize SceMi */
	init();
	
	/* Start test */
	simulate();
};
/**********************************************************************************************************************************************/
/*
 * TestBench class destructor.
 * */
TestBench::~TestBench() 
{
	SceMi::Shutdown( SceMi::Pointer() );
	if( NULL != m_params )
		delete m_params;
	
	if( NULL != m_inPortMessage_mp_out )
		delete m_inPortMessage_mp_out;
}

/**********************************************************************************************************************************************/
/*
 * Init function used to prepare SceMi infrastructure. 
 * */
void TestBench::init( void ) 
{
	/* Register Info and Error Callback functions in SceMi infrastructure */
	SceMi::RegisterInfoHandler( info_handler, NULL );
	SceMi::RegisterErrorHandler( error_handler, NULL );

	/* Create SceMiParameters object */
	m_params = new SceMiParameters( m_scemiFile );							
	
	if ( !VERBOSE ) 
		cout << "SceMiParameters loaded from file " << m_scemiFile << endl;

	/* Initialize SceMi */
	if ( SceMi::Init( SceMi::Version("2.0" ), m_params, NULL ) == NULL ) 
	{
		cout << "Scemi is not initialized successfully " << endl;
		return;
	}
	else 
	{
		cout << "Scemi initialized successfully " << endl;
		/* Display information about transactors, ports and clocks */
		this->logParamsInfo();
		/* Bind Message ports with proxy */
		this->bindCallbacks();

		/* Create SceMiMessageData object used to send messages to the transactor */
		m_inPortMessage_mp_out = new SceMiMessageData( *m_messageInPortProxy_mp_out, NULL ); 
	}
}
/**********************************************************************************************************************************************/
/*
 * Functions to bind Message ports with proxy.
 * */
void TestBench::bindCallbacks( void ) 
{
	const SceMiMessageInPortBinding messageInPortProxy_inBinding = { this, messageInPortProxyIsReadyCallback, NULL };
	m_messageInPortProxy_mp_out = SceMi::Pointer()->BindMessageInPort( "U_generic_bus_0", "U_SceMiMessageInPort_0", &messageInPortProxy_inBinding );

	const SceMiMessageOutPortBinding messageOutPortProxy_outBinding = { this, &messageOutPortProxyreceiveCallback, NULL };
	m_messageOutPortProxy_mp_in = SceMi::Pointer()->BindMessageOutPort( "U_generic_bus_0", "U_SceMiMessageOutPort_0", &messageOutPortProxy_outBinding, NULL );
}
/**********************************************************************************************************************************************/
/*
 * Function to send message to transactor
 * */
void TestBench::sendMsgToTransactor( unsigned char _control, unsigned long long _data ) 
{
	unsigned int msg = 0;

	msg = (unsigned int)_data;

	/*Check if transactor is ready to receive a new message */
	while( m_inPortReady == 0 )
	{
		SceMi::Pointer()->ServiceLoop(); 
	}
	m_inPortReady--;
	/*Prepare message to send to transactor*/
	m_inPortMessage_mp_out->Set( 0, (SceMiU32)msg);
	msg = (unsigned int)_control;
	msg = (msg << 8) | (unsigned int)(_data >> 32);
	m_inPortMessage_mp_out->Set( 1, (SceMiU32)msg);
	/*Send message to transactors*/
	m_messageInPortProxy_mp_out->Send( *m_inPortMessage_mp_out );
}
/**********************************************************************************************************************************************/
/*
 * Function to write data to memory
 * */
unsigned long long TestBench::writeData( unsigned long long _data ) 
{
	sendMsgToTransactor(0x01, _data);

	while( m_outPortDataReady == 0 )
	{
		SceMi::Pointer()->ServiceLoop();
	}
	m_outPortDataReady--;
	return m_readData;
}
/**********************************************************************************************************************************************/
/*
 * Function to read data from memory.
 * */
unsigned long long TestBench::readData( unsigned char _control ) 
{
	sendMsgToTransactor( _control, 0x00);

	/*Check if a new data is ready to use */
	while( m_outPortDataReady == 0 )
	{
		SceMi::Pointer()->ServiceLoop(); 
	}
	m_outPortDataReady--;
	return m_readData;
}
/**********************************************************************************************************************************************/
/*
 * Test application
 * */
void TestBench::simulate( void ) 
{
	
	unsigned long long data;
	unsigned long long empty = 0;
	unsigned long long full = 0;

	if( (readData(0x02) >> 40) & 1 )	//Check first read when FIFO is empty
		cout << " FIFO is empty: could not read" << endl;
	else
		cout << " Error: FIFO not initialized" << endl;

	cout << "Write data to HES board by scemi!" << endl;
	for ( unsigned long long i = 0; i < 257; ++i ) 
	{
		full = (writeData( i+100 ) >> 41);
		cout << " Write data: " << i + 100 << endl;
		if(full == 1)
		{
			cout << " Error: FIFO is full" << endl;
		}
	}
	
	if( writeData(0xdeadbeef) >> 41 )
	{
		cout << " Write data: 0xdeadbeef" << endl;
		cout << " FIFO is full" << endl;
	}
	else
		cout << " ERROR: FIFO not full after write loop" << endl;

	cout << "Read data from HES board by scemi!" << endl;
	for ( unsigned long long i = 0; i < 257; ++i ) 
	{
		data = readData(0x02);		//Read 64 bits of data (only 42 bits useful)
//		cout << "Received data from readData() is: " << data << endl;
		empty = (data >> 40) & 1;	//Check if FIFO is empty
//		cout << "Transformation for 'empty' is: " << empty << endl;
		data = (data << 24);
		data = (data >> 24);
//		cout << "Transformation for 'data' is: " << data << endl;
		if( data != i + 100 )
		{
			cout << "ERROR: Read: " << data << " expected: " << i + 100 <<endl;
		}
		else
			cout << " Read data: " <<  data << endl;

		if(1 == empty)
		{
			cout << " FIFO is now empty" << endl;
		}
	}

	this->shutDown();
}
/**********************************************************************************************************************************************/
/*
 * Function to display information about transactors, message ports and clocks.
 * */
void TestBench::logParamsInfo( void )
{
	int inCount = SceMiParametersNumberOfObjects( m_params, "MessageInPort", NULL );
	int outCount = SceMiParametersNumberOfObjects( m_params, "MessageOutPort", NULL );
	int clkCount = SceMiParametersNumberOfObjects( m_params, "Clock", NULL );
	
	if ( !VERBOSE ) 
	{
		cout << endl << "Number of SceMi Message In Ports in the design:  " << inCount << endl;
	}
	for( int i = 0; i < inCount; ++i )
	{
		if ( !VERBOSE ) 
		{
			cout << "\tport name:         " << SceMiParametersAttributeStringValue( m_params, "MessageInPort", i, "PortName", NULL) << endl;
			cout << "\tport width:        " << SceMiParametersAttributeIntegerValue( m_params, "MessageInPort", i, "PortWidth", NULL) << endl;
			cout << "\tport address:      " << SceMiParametersAttributeIntegerValue( m_params, "MessageInPort", i, "PortAddress", NULL) << endl << endl;
		}	
	}

	if ( !VERBOSE ) 
	{
		cout << endl << "Number of SceMi Message Out Ports in the design: " << outCount << endl;
	}
	
	for( int i = 0; i < outCount; ++i )
	{
		if ( !VERBOSE ) 
		{
			cout << "\tport name:         " << SceMiParametersAttributeStringValue( m_params, "MessageOutPort", i, "PortName", NULL) << endl;
			cout << "\tport width:        " << SceMiParametersAttributeIntegerValue( m_params, "MessageOutPort", i, "PortWidth", NULL) << endl;
			cout << "\tport address:      " << SceMiParametersAttributeIntegerValue( m_params, "MessageOutPort", i, "PortAddress", NULL) << endl << endl;
		}		
	}
	
	if ( !VERBOSE ) 
	{
		cout << endl << "Number of SceMi Clock Ports in the design:       " << clkCount << endl;
	}
	
	for( int i = 0; i < clkCount; ++i )
	{
		if ( !VERBOSE ) 
		{
			cout << "\tPort Name:         " << SceMiParametersAttributeStringValue( m_params, "Clock", i, "ClockName", NULL) << endl;
			cout << "\tRatio Numerator:   " << SceMiParametersAttributeIntegerValue( m_params, "Clock", i, "RatioNumerator", NULL) << endl;
			cout << "\tRatio Denominator: " << SceMiParametersAttributeIntegerValue( m_params, "Clock", i, "RatioDenominator", NULL) << endl;
			cout << "\tDuty Hi:           " << SceMiParametersAttributeIntegerValue( m_params, "Clock", i, "DutyHi", NULL) << endl;
			cout << "\tDuty Lo:           " << SceMiParametersAttributeIntegerValue( m_params, "Clock", i, "DutyLo", NULL) << endl;
			cout << "\tPhase:             " << SceMiParametersAttributeIntegerValue( m_params, "Clock", i, "Phase", NULL) << endl;
			cout << "\tReset Cycles:      " << SceMiParametersAttributeIntegerValue( m_params, "Clock", i, "ResetCycles", NULL) << endl << endl;	
		}		
	}
}
/**********************************************************************************************************************************************/
/*
 * Function to shutdown scemi
 * */
void TestBench::shutDown( void ) 
{
	SceMi::Shutdown( SceMi::Pointer() );
}
/**********************************************************************************************************************************************/
/*
 * Info callback function
 * */
void info_handler(void* context, SceMiIC* ic) 
{
	cout << endl << "SCEMI Info: " << ic->Message << endl;			
}
/**********************************************************************************************************************************************/
/*
 * Error callback function
 * */
void error_handler(void* context, SceMiEC* ec) 
{
	cout << endl << "SCEMI Error: " << ec->Message << endl;
	SceMi::Shutdown(SceMi::Pointer());																								
}
