#include <scemi.h>
#include <iostream>
#include <fstream>
#include <string>

#define VERBOSE 1

using namespace std;

void info_handler (void* context, SceMiIC* ic);
void error_handler(void* context, SceMiEC* ec);

class TestBench 
{
	public:
		SceMiParameters *m_params;

		SceMiMessageInPortProxy *m_messageInPortProxy_mp_out;
		SceMiMessageData	 *m_inPortMessage_mp_out;

		SceMiMessageOutPortProxy *m_messageOutPortProxy_mp_in;

		unsigned long long m_readData;
		const char *m_scemiFile;
		
		unsigned int m_inPortReady;
		unsigned int m_outPortDataReady;

		void init( void );
		void bindCallbacks( void );
		void simulate( void );
		unsigned long long writeData( unsigned long long _data );
		unsigned long long readData( unsigned char _control );
		void sendMsgToTransactor( unsigned char _control, unsigned long long _data );
		void service( void );
		void logParamsInfo( void );
		void shutDown( void );

		TestBench( const char * _scemiXmlFile );

		~TestBench() ;
};
