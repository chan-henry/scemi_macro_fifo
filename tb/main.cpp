#include "scemi.h"
#include <iostream>
#include <fstream>
#include "TestBench.h"

using namespace std;

int main ( int argc, char* argv[] ) 
{
	cout << "start scemi testbench" << endl;
	
	char * scemiFile = 0;
	int flag = 0;
	
	if ( argc == 3 ) 
	{
		for ( int i = 1; i < argc; ++i ) 
		{
			std::string param = argv[i];
			if ( param.find("-s") != std::string::npos ) 
			{
				flag += 1;
				scemiFile = argv[++i];
				cout << endl << "XML file: " << scemiFile << endl;
			}
		}
		
		if ( flag != 1 ) 
		{
			cout << "Error: Please use as: testbench -s scemi.xml " << endl;
			return (0);
		}
	}
	else 
	{
		cout << "Error: Please use as: testbench -s scemi.xml " << endl;
		return (0);
	}
	
	TestBench TB( scemiFile );
	
	cout << "end of scemi testbench" << endl;

	return (0);
}
