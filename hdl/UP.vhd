library ieee;
use ieee.std_logic_1164.all;
--library SceMi;
--use SceMi.SceMiMacros.all;

entity UP is
end UP;

architecture arch_top_scemi of UP is
	component generic_bus is
		port(
			full			: in std_logic;
			empty			: in std_logic;

			wr_n			: out std_logic;
			rd_en			: out std_logic;
			data_in			: out std_logic_vector (39 downto 0);
			data_out		: in std_logic_vector (39 downto 0)
		);
	end component;
	
	component fifo is
	generic(
		DATA_WIDTH	: positive := 40
	);
	port(
		clk	: in	std_logic;
		rst	: in	std_logic;
		writeen	: in	std_logic;
		datain	: in	std_logic_vector (DATA_WIDTH-1 downto 0);
		readen	: in	std_logic;
		dataout	: out	std_logic_vector (DATA_WIDTH-1 downto 0);
		empty	: out	std_logic;
		full	: out	std_logic
	);
	end component;
	
	component SceMiClockPort
        generic(
            ClockNum			: natural := 1;
            RatioNumerator		: natural := 1;
            RatioDenominator		: natural := 1;
            DutyHi			: natural := 0;
            DutyLo			: natural := 100;
            Phase			: natural := 0;
            ResetCycles			: natural := 100 
		);
        port(
            Cclock			: out std_logic;
            Creset			: out std_logic 
		);
    end component;
	
	signal S_data_in	: std_logic_vector (39 downto 0);
	signal S_data_out	: std_logic_vector (39 downto 0);
	signal S_we_n		: std_logic;
	signal S_clk		: std_logic;
	signal S_clk_out	: std_logic;
	signal S_rst		: std_logic;
	
	
	signal S_cclock 	: std_logic;
	signal S_creset 	: std_logic;

	signal S_readen		: std_logic;

	signal S_full		: std_logic;
	signal S_empty		: std_logic;
	
begin 
	U_generic_bus_0:  generic_bus
		port map(
			---------- user ports ----------
			full			=> S_full,
			empty			=> S_empty,

			data_out		=> S_data_out,
			data_in			=> S_data_in,	   
			rd_en			=> S_readen,
			wr_n			=> S_we_n
		);
	

	U_top_1 : fifo
		generic map(
			DATA_WIDTH => 40
		)
		port map(
			clk	=> S_clk,
			rst	=> S_rst,
			writeen	=> S_we_n,
			datain	=> S_data_in,
			readen	=> S_readen,
			dataout	=> S_data_out,
			empty	=> S_empty,
			full	=> S_full
		);

	S_rst		<= S_creset;
	S_clk		<= S_cclock;
	
	U_SceMiClockPort_0: SceMiClockPort
		generic map (
            RatioNumerator	=> 1,
            RatioDenominator	=> 1,
            DutyHi		=> 20,
            DutyLo		=> 80,
            Phase		=> 0,
            ResetCycles		=> 100, 
	    ClockNum		=> 0
		)
        port map (
            Cclock		=> S_cclock,
            Creset		=> S_creset
		);
end arch_top_scemi;
