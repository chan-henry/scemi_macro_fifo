library IEEE;
use IEEE.std_logic_1164.all;

entity transactor_core is
	port(
		-- generic bus
		wr_n			: out std_logic;
		rd_en			: out std_logic;

		full			: in std_logic;
		empty			: in std_logic;

		data_in			: out std_logic_vector(39 downto 0);	
		data_out		: in std_logic_vector(39 downto 0);
			 		
		-- sce-mi					
		ureset			: in std_logic;
		uclock			: in std_logic;
		clock_en		: in std_logic;
		ready_for_cclock	: out std_logic;
	
		mp_in_rx_rdy	 	: out std_logic;
		mp_in_tx_rdy		: in std_logic;
		mp_in_msg		: in std_logic_vector(41 downto 0);
		
		mp_out_rx_rdy		: in std_logic;
		mp_out_tx_rdy		: out std_logic;
		mp_out_msg	 	: out std_logic_vector(41 downto 0)
	);
end;

architecture synthesis of transactor_core is
	constant C_FULL_BIT	: natural := 41;
	constant C_EMPTY_BIT	: natural := 40;
	constant C_RD_EN_BIT	: natural := 41;
	constant C_WR_N_BIT	: natural := 40;
	constant C_DATA_IN_H	: natural := 39;
	constant C_DATA_IN_L	: natural := 0;	
	
	type T_state is (IDLE, STIMULATE, RESPOND);
	signal S_state : T_state;
	
begin
	process(uclock, ureset)	--Asynchronous reset
	begin
		if (ureset = '1') then	--Initialize all values on reset
			ready_for_cclock <= '0';
			S_state <= IDLE;
			wr_n <= '0';
			rd_en <= '0';
			data_in <= (others => '0');			
			mp_in_rx_rdy	<= '1';
			mp_out_tx_rdy	<= '0';	
			mp_out_msg <= (others => '0');
		elsif (rising_edge(uclock)) then
			case S_state is
			when IDLE =>
				-- Check for incoming msgs from the SW TB
				mp_out_tx_rdy		<= '0';
				if ('1' = mp_in_tx_rdy) then
					mp_in_rx_rdy	<= '0';
					wr_n		<= mp_in_msg(C_WR_N_BIT);
					rd_en		<= mp_in_msg(C_RD_EN_BIT);
					data_in		<= mp_in_msg(C_DATA_IN_H downto C_DATA_IN_L);
					S_state		<= STIMULATE;

					ready_for_cclock	<= '1';
				else
					S_state		<= IDLE;
				end if;
			when STIMULATE =>
				if('1' = clock_en) then
					S_state						<= RESPOND;
					ready_for_cclock				<= '0';
				else
					S_state <= STIMULATE;
				end if;
			when RESPOND =>
				if('1' = mp_out_rx_rdy) then
					mp_out_tx_rdy					<= '1';
					mp_out_msg(C_DATA_IN_H downto C_DATA_IN_L)	<= data_out;
					mp_out_msg(C_FULL_BIT)				<= full;
					mp_out_msg(C_EMPTY_BIT)				<= empty;
					mp_in_rx_rdy					<= '1';
					S_state		<= IDLE;
				else
					S_state		<= RESPOND;
				end if;
			end case;
		end if;
	end process;	
	
end;
