library ieee;
use ieee.std_logic_1164.all;
--library SceMi;
--use SceMi.SceMiMacros.all;

entity generic_bus is
	port(
		full			: in std_logic;
		empty			: in std_logic;

		wr_n			: out std_logic;
		rd_en			: out std_logic;
		data_in			: out std_logic_vector (39 downto 0);
		data_out		: in std_logic_vector (39 downto 0)
	);
end;

architecture synthesis of generic_bus is

	component transactor_core is
		port(
			-- generic bus				   		
			wr_n			: out std_logic;
			rd_en			: out std_logic;

			full			: in std_logic;
			empty			: in std_logic;

			data_in			: out std_logic_vector(39 downto 0);	
			data_out		: in std_logic_vector(39 downto 0);
				 		
			-- sce-mi					
			ureset			: in std_logic;
			uclock			: in std_logic;		
			clock_en		: in std_logic;
			ready_for_cclock	: out std_logic;
		
			mp_in_rx_rdy	 	: out std_logic;
			mp_in_tx_rdy		: in std_logic;
			mp_in_msg		: in std_logic_vector(41 downto 0);
			
			mp_out_rx_rdy		: in std_logic;
			mp_out_tx_rdy		: out std_logic;
			mp_out_msg		: out std_logic_vector(41 downto 0)
		);
	end component;
	
	component SceMiClockControl
        generic( 
		ClockNum		: natural := 1 
		);
        port(
		Uclock			: out std_logic;
		Ureset			: out std_logic;
		ReadyForCclock		: in std_logic;
		CclockEnabled		: out std_logic;
		ReadyForCclockNegEdge	: in std_logic;
		CclockNegEdgeEnabled	: out std_logic 
		);
    end component; 
	
	component SceMiMessageInPort
        generic( 
		PortWidth		: natural := 42
		);
        port(
		ReceiveReady		: in  std_logic;
		TransmitReady		: out std_logic;
		Message			: out std_logic_vector(PortWidth - 1 downto 0) 
		);
    end component;

    component SceMiMessageOutPort
        generic( 
		PortWidth		: natural := 42;
		PortPriority		: natural := 10 
		);
        port(
            TransmitReady		: in std_logic;
            ReceiveReady		: out std_logic;
            Message			: in std_logic_vector(PortWidth - 1 downto 0) 
		);
    end component;
		
	signal S_uclock 		: std_logic;
	signal S_ureset 		: std_logic;
	signal S_clock_en		: std_logic;
	signal S_rfCclock		: std_logic;
	signal S_mp_in_rx_rdy		: std_logic;
	signal S_mp_in_tx_rdy		: std_logic;
	signal S_mp_in_msg		: std_logic_vector(41 downto 0);			
	signal S_mp_out_rx_rdy		: std_logic;
	signal S_mp_out_tx_rdy		: std_logic;
	signal S_mp_out_msg	 	: std_logic_vector(41 downto 0);
	signal S_ready_for_cclock	: std_logic;
	
begin	
	U_SceMiClockControl_0: SceMiClockControl
		generic map (
			ClockNum		=> 0
		)
		port map (
			Uclock			=> S_uclock,
			Ureset			=> S_ureset,
			ReadyForCclock		=> S_ready_for_cclock,
			CclockEnabled		=> S_clock_en,
			ReadyForCclockNegEdge	=> '1',
			CclockNegEdgeEnabled	=> open
		);
	
	U_SceMiMessageInPort_0: SceMiMessageInPort
		generic map (
			PortWidth		=> 42
		)
		port map (
			ReceiveReady		=> S_mp_in_rx_rdy,
			TransmitReady		=> S_mp_in_tx_rdy,
			Message			=> S_mp_in_msg
		);
		
	U_SceMiMessageOutPort_0: SceMiMessageOutPort
		generic map (
			PortWidth 		=> 42
		)
		port map (
			ReceiveReady 		=> S_mp_out_rx_rdy,
			TransmitReady 		=> S_mp_out_tx_rdy,
			Message 		=> S_mp_out_msg
		); 
		
	U_transactor_core_0 : transactor_core
		port map (			
			wr_n			=> wr_n,
			rd_en			=> rd_en,

			full			=> full,
			empty			=> empty,

			data_in			=> data_in,
			data_out		=> data_out,			 
			
			ureset			=> S_ureset,
			uclock			=> S_uclock,			
			clock_en		=> S_clock_en,
			ready_for_cclock	=> S_ready_for_cclock,
					
			mp_in_rx_rdy		=> S_mp_in_rx_rdy,
			mp_in_tx_rdy		=> S_mp_in_tx_rdy,
			mp_in_msg		=> S_mp_in_msg,
			
			mp_out_rx_rdy		=> S_mp_out_rx_rdy,
			mp_out_tx_rdy		=> S_mp_out_tx_rdy,
			mp_out_msg		=> S_mp_out_msg
		);
end;
